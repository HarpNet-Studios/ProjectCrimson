This directory contains "packages" of textures and other materials
for use with the engine, in their respective subdirectories.

Package folder starting with a "!" denote that they are not yet implemented.
