#include "engine.h"

#include "LuaInterface.h"

void initLUA()
{
	LuaInterface::startLua();
	LuaInterface::stopLua();
}
COMMAND(initLUA, "");