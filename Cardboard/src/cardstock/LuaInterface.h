#pragma once

#include "../lua/lua.hpp"
#include "engine.h"

namespace LuaInterface
{
	enum DataType
	{
		Integer,
		Double,
		String
	};

	struct LightDataPiece
	{
		LightDataPiece(void* p, const char* n, DataType dt)
			: ptr(p),
			name(n),
			dataType(dt)
		{ }
		void* ptr;
		const char* name;
		DataType dataType;
	};

	static vector<LightDataPiece> lightUserDataList;

	static lua_State* L;
	static char loadedLuaFiles[1024]; //Preallocate 2048 chars for script names
	static char registeredFunctions[2048]; //Preallocate 4096 chars for function names	

	extern void startLua();
	extern void stopLua();

	extern int findStringInArray(const char* array, const char* stringToFind);
	extern bool loadLuaFile(const char* filename);
	extern void registerFunction(const char* desiredName, lua_CFunction func);
	extern bool checkIsRegistered(const char* desiredName);
	extern int getNumberOfValuesOnStack();
	
	extern void callLuaFunction(int numOfArgs, int numOfReturns);

	extern double popStackAsDouble();
	extern int popStackAsInteger();
	extern const char* popStackAsString();
	
	extern void popStack(int vals);
	extern void clearStack();

	extern double peekStackAsDouble(int index);
	extern int peekStackAsInteger(int index);
	extern const char* peekStackAsString(int index);

	extern int peekStackType(int index);

	extern void pushStackWithDouble(double num);
	extern void pushStackWithInteger(int num);
	extern void pushStackWithString(const char* string);
	extern void pushStackWithLuaFunction(const char* function);
	extern void pushStackWithLuaGlobal(const char* global);

	extern void createGlobalTable(const char* libName, const luaL_Reg arraylib[]);
	extern void checkLuaArgument(bool condition, int numberOfArg, const char* errorMessage);
	extern void addLightUserData(void* ptr, const char* name, DataType dataType);
};