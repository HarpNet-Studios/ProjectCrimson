#include "LuaInterface.h"

namespace LuaInterface
{

	/*
	**Retrieves the value of the provided variable
	**
	**@param The lua state, automatically passed
	**       when called by Lua
	**
	**@return Returns the number of values this
	**        function pushed on the stack, needed
	**        by Lua
	*/
	extern "C" static int getDataPiece(lua_State* L)
	{
		const char* name = luaL_checkstring(L, 1); //Get Argument 1

		//Find the data piece requested in our ludl
		LightDataPiece* data = nullptr;
		for (int i = 0; i != lightUserDataList.length(); i++)
			if (strcmp(lightUserDataList.get(i).name, name) == 0)
				data = &lightUserDataList.get(i);

		if (data == nullptr)
			return 0;

		//Push the value with the correct type onto the Lua stack
		switch (data->dataType)
		{
		case DataType::Integer:
			pushStackWithInteger(*((int*)(data->ptr)));
			break;
		case DataType::Double:
			pushStackWithDouble(*((double*)(data->ptr)));
			break;
		case DataType::String:
			pushStackWithString((const char*)(data->ptr));
			break;
		}

		return 1;
	}

	/*
	**Sets the value of the provided variable
	**
	**@param The lua state, automatically passed
	**       when called by Lua
	**
	**@return Returns the number of values this
	**        function pushed on the stack, needed by Lua
	*/
	extern "C" static int setDataPiece(lua_State* L)
	{
		const char* name = luaL_checkstring(L, 1); //Argument 1

		LightDataPiece* data = nullptr;

		for (int i = 0; i < lightUserDataList.length(); i++)
		{
			if (strcmp(lightUserDataList.get(i).name, name) == 0)
				data = &lightUserDataList.get(i);
		}

		if (data == nullptr)
		{
			return 0;
		}

		switch (data->dataType)
		{
		case DataType::Integer:
			*((int*)(data->ptr)) = luaL_checkinteger(L, 2); //Argument 2
			break;
		case DataType::Double:
			*((double*)(data->ptr)) = luaL_checknumber(L, 2); //Argument 2
			break;
		case DataType::String:
		{
			const char* val = luaL_checkstring(L, 2); //Argument 2
			copystring((char*)(data->ptr), val, strlen(val) + strlen((char*)(data->ptr)) + 1);
			break;
		}
		}

		return 0;
	}

	static const struct luaL_Reg lightUserData[] =
	{
		{ "getData", getDataPiece },
		{ "setData", setDataPiece },
		{ NULL, NULL }
	};

	/*
	**Initializes Lua
	*/
	void startLua()
	{
		L = luaL_newstate();
		createGlobalTable("LUDL", lightUserData);
	}

	/*
	**Closes Lua thread
	*/
	void stopLua()
	{
		lua_close(L);
	}

	/*
	**Tries to find a given string in a given CString array
	**
	**@param The array to search for the string in
	**@param The string to search for in the array
	**
	**@return Returns the index of the next available spot in the
	**        CString array, or -1 if the provided string was found in
	**        the provided array
	*/
	int findStringInArray(const char* array, const char* stringToFind)
	{
		string currentString = "";

		int index = -1;
		do
		{
			index++;
			copystring(currentString, "", 1);

			//Load each character individually into the string until
			//A nil character is found
			int stringBufferLength = 1;
			while (array[index] != '\0')
			{
				concatstring(currentString, &array[index], ++stringBufferLength);
				index++;
			}

			if (strcmp(stringToFind, currentString) == 0) return -1;
		} while (array[index + 1] != '\0'); //As long as there's still more files

		return index;
	}

	/*
	**Checks if the provided Lua file has
	**already been loaded. If not, it is
	**loaded into the Lua state
	**
	**@param The name of the Lua file to
	**       be loaded
	**
	**@return Returns false if the file
	**        has already been loaded,
	**        true otherwise
	*/
	bool loadLuaFile(const char* filename)
	{
		int index = findStringInArray(loadedLuaFiles, filename);
		if (index == -1) return false;
		if (index != 0) index++;

		int length = strlen(filename) + 1;
		for (int i = 0; i < length; i++) loadedLuaFiles[index++] = filename[i];

		string fullPath = "scripts/";
		concatstring(fullPath, filename, length + 8);
		concatstring(fullPath, ".lua", length + 12);

		luaL_dofile(L, fullPath);

		return true;
	}

	/*
	**Checks if the provided function has already been loaded. If not,
	**it is loaded into the Lua state
	**
	**@param The name Lua will call when requesting this function
	**@param The function lua will call
	*/
	void registerFunction(const char* desiredName, lua_CFunction func)
	{
		if (checkIsRegistered(desiredName))
			lua_register(L, desiredName, func);
	}

	/*
	**Checks if the desired function or function,
	**name has already been loaded
	**
	**@param The name you would like lua to call
	**       when using this function
	**
	**@return Returns false if the function has
	**        already been registered, true otherwise
	*/
	bool checkIsRegistered(const char* desiredName)
	{
		int index = findStringInArray(registeredFunctions, desiredName);
		if (index == -1) return false;
		if (index != 0) index++;

		for (int i = 0; i < strlen(desiredName) + 1; i++) registeredFunctions[index++] = desiredName[i];

		return true;
	}

	/*
	**@return Returns the number
	**        of values on the Lua
	**        stack. Commonly used
	**        to see how many values
	**        were passed to a function
	*/
	int getNumberOfValuesOnStack()
	{
		return lua_gettop(L);
	}

	/*
	**Calls the function currently pushed on the stack with
	**however many arguments were pushed onto the stack
	**
	**@param Number of arguments in the function being called
	**@param Number of values the lua function will return
	*/
	void callLuaFunction(int numOfArgs, int numOfReturns)
	{
		lua_call(L, numOfArgs, numOfReturns);
	}

	/*
	**@return Returns the number at the top
	**        of the stack as a double
	*/
	double popStackAsDouble()
	{
		double val = peekStackAsDouble(1);
		popStack(1);

		return val;
	}

	/*
	**@return Returns the number at the top
	**        of the stack as a integer
	*/
	int popStackAsInteger()
	{
		int val = peekStackAsInteger(1);
		popStack(1);

		return val;
	}

	/*
	**@return Returns the value at the top
	**        of the stack as a string
	*/
	const char* popStackAsString()
	{
		const char* val = peekStackAsString(1);
		popStack(1);

		return val;
	}

	/*
	**Pops the lua stack x amount of times
	**depending on the provided number of
	**vals
	**
	**@param The number of vals to pop from
	**       the stack
	*/
	void popStack(int vals)
	{
		lua_pop(L, vals);
	}

	/*
	**Clears all values off
	**of the Lua stack
	*/
	void clearStack()
	{
		popStack(getNumberOfValuesOnStack());
	}

	/*
	**Shows lua stack at the given index,
	**returning it as a double without removing
	**the value from the stack
	**
	**@param The index of the value to peek
	**
	**@return Returns the value at the index
	**        as a double
	*/
	double peekStackAsDouble(int index)
	{
		return luaL_checknumber(L, index);
	}

	/*
	**Shows lua stack at the given index,
	**returning it as an integer without removing
	**the value from the stack
	**
	**@param The index of the value to peek
	**
	**@return Returns the value at the index
	**        as an integer
	*/
	int peekStackAsInteger(int index)
	{
		return luaL_checkinteger(L, index);
	}

	/*
	**Shows lua stack at the given index,
	**returning it as a string without removing
	**the value from the stack
	**
	**@param The index of the value to peek
	**
	**@return Returns the value at the index
	**        as a string
	*/
	const char* peekStackAsString(int index)
	{
		return luaL_checkstring(L, index);
	}

	/*
	**Returns the lua type of the
	**value at the provided index
	**
	**@param The index of the value to peek
	**
	**@return Returns the type of
	**        the found value
	*/
	int peekStackType(int index)
	{
		return lua_type(L, index);
	}

	/*
	**Pushes the given value onto the top
	**of the stack as a double
	**
	**@param The double to be pushed onto
	**       the stack
	*/
	void pushStackWithDouble(double num)
	{
		lua_pushnumber(L, num);
	}

	/*
	**Pushes the given value onto the top
	**of the stack as an integer
	**
	**@param The integer to be pushed
	**       onto the stack
	*/
	void pushStackWithInteger(int num)
	{
		lua_pushinteger(L, num);
	}

	/*
	**Pushes the given value onto the top of the
	**stack as a string
	**
	**@param The string to be pushed onto the stack
	*/
	void pushStackWithString(const char* string)
	{
		lua_pushstring(L, string);
	}

	/*
	**Pushes the Lua function with the given name onto
	**the stack
	**
	**@param The name of the Lua function to be pushed onto
	**       the stack
	*/
	void pushStackWithLuaFunction(const char* function)
	{
		pushStackWithLuaGlobal(function);
	}

	/*
	**Pushes the Lua global with the given name onto
	**the stack
	**
	**@param The name of the Lua global to be pushed onto
	**       the stack
	*/
	void pushStackWithLuaGlobal(const char* global)
	{
		lua_getglobal(L, global);
	}

	/*
	**Creates a global metatable in Lua. Global metatables are accessed like such:
	**libraryName.function(args, ...)
	**
	**This is mainly used when creating full user data
	**
	**It is the Lua writer's responsibility to call:
	**local libraryName = require "libraryName"
	**
	**@param The desired name for the library
	**@param The array of mini array's containing the function names and the functions
	**       themselves
	*/
	void createGlobalTable(const char* libName, const luaL_Reg arrayLib[])
	{
		lua_newtable(L);
		luaL_setfuncs(L, arrayLib, 0);
		lua_setglobal(L, libName);
	}

	/*
	**Provides lua with a message it can give to the caller of the c function if the argument
	**provided doesn't meet a certain condition
	**
	**@param The condition lua should check for
	**@param The position on the stack the argument is on
	**@param The error message lua should display if the argument does not pass the condition
	*/
	void checkLuaArgument(bool condition, int numberOfArg, const char* errorMessage)
	{
		luaL_argcheck(L, (int)condition, numberOfArg, errorMessage);
	}

	/*
	**Adds a light user data entry to the LUDL, given the pointer to the
	**variable, the desired name for the variable, and the variable's
	**datatype. Lua accesses this variable as if it were a Lua global
	**
	**@param The pointer to the variable to be added
	**@param The desired name for the Lua global that references this variable
	**@param The data type of the variable being added
	*/
	void addLightUserData(void* ptr, const char* name, DataType dataType)
	{
		lightUserDataList.add(LightDataPiece(ptr, name, dataType));
	}
}